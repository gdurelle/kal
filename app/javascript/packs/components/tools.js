let ajax = (url, callback, method='GET') =>{
  var request = new XMLHttpRequest();
  request.open(method, url, true);
  request.onload = function() {
    if (this.status >= 200 && this.status < 400) {
      // Success!
      callback(this.response);
    } else {
      // We reached our target server, but it returned an error

    }
  };
  request.onerror = function() {
    // There was a connection error of some sort
  };
  request.send();
}

let darker = (hexa_color)=>{
  let color = hexa_color.substring(1, 7);
  let rgb_total = parseInt(color, 16);
  let darken_val = 10;

  let r = Math.abs( ((rgb_total >> 16) & 0xFF)-darken_val );
  if(r>255) r=r-(r-255);
  r = Number(r<0||isNaN(r)) ? 0 : ((r>255) ? 255 : r).toString(16);
  if(r.length == 1) r = '0' + r;

  let g = Math.abs( ((rgb_total >> 8) & 0xFF)-darken_val );
  if(g>255) g=g-(g-255);
  g = Number(g<0||isNaN(g)) ? 0 : ((g>255) ? 255 : g).toString(16);
  if(g.length == 1) g = '0' + g;

  let b = Math.abs( (rgb_total & 0xFF)-darken_val );
  if(b>255) b=b-(b-255);
  b = Number(b<0||isNaN(b)) ? 0 : ((b>255) ? 255 : b).toString(16);
  if(b.length == 1) b = '0' + b;

  let darker = '#'+r+g+b;
  return darker;
}

let matchHeight = () =>{
  //Grab divs with the class name 'match-height'
  var getDivs = document.getElementsByClassName('match-height');

  //Find out how my divs there are with the class 'match-height'
  var arrayLength = getDivs.length;
  var heights = [];

  //Create a loop that iterates through the getDivs variable and pushes the heights of the divs into an empty array
  for (var i = 0; i < arrayLength; i++) {
      heights.push(getDivs[i].offsetHeight);
  }

   //Find the largest of the divs
  function getHighest() {
    return Math.max(...heights);
  }

  //Set a variable equal to the tallest div
  var tallest = getHighest();

  //Iterate through getDivs and set all their height style equal to the tallest variable
  for (var i = 0; i < getDivs.length; i++) {
      getDivs[i].style.height = tallest + "px";
  }
}

export {ajax, darker, matchHeight};

document.addEventListener('turbolinks:load', () => {

  let meeting = document.getElementById('event_type_is_meeting');
  let available = document.getElementById('event_type_is_available');
  let offtime = document.getElementById('event_type_is_offtime');

  if(meeting != null){
    meeting.addEventListener('change', () => {
      if(meeting.checked){
        available.checked = false;
        offtime.checked = false;
      }else{
        available.checked = true;
        offtime.checked = true;
      }
    });
  }

  if(available != null){
    available.addEventListener('change', () => {
      if(available.checked){
        meeting.checked = false;
        offtime.checked = false;
      }else{
        meeting.checked = true;
        offtime.checked = true;
      }
    });
  }

  if(offtime != null){
    offtime.addEventListener('change', () => {
      if(offtime.checked){
        meeting.checked = false;
        available.checked = false;
      }else{
        meeting.checked = true;
        available.checked = true;
      }
    });
  }

});

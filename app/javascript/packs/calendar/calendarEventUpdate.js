// calendarEventUpdate
export default function calendarEventUpdate(eventInfo){
  let start = eventInfo.event.start;
  let end = eventInfo.event.end;
  let slot_id = eventInfo.event.id;
  let user = document.getElementById('user');
  let user_slug = user.getAttribute('data-slug');

  $.ajax({
    url: '/user/'+user_slug+'/slots/'+slot_id+'.json',
    method: 'PUT',
    dataType: 'json',
    data: {
      slot: {
        date_start: start,
        date_end: end
      }
    },
    success: (data, status, xhr) =>{
    }
  });
}

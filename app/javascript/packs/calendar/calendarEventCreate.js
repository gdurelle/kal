// calendarEventCreate
let creating = false;

export default function calendarEventCreate(calendar){
  let user = document.getElementById('user');
  let user_slug = user.getAttribute('data-slug');
  let event_type_id = null;
  let event_type_label = null;
  let selected_attended = null;
  let attendee_id = null;

  let event_type_select = document.getElementById('event_type');
  if(event_type_select.selectedOptions.length > 0){
    event_type_id = event_type_select.selectedOptions[0].value;
    event_type_label = event_type_select.selectedOptions[0].text;
  }

  if (!creating) {
    let selected_option = event_type_select.options[event_type_select.selectedIndex];
    let attendee_select = document.getElementById('attendee_id');
    if(attendee_select != null)
    if(!['availability', 'offtime'].includes(selected_option.getAttribute('data-type'))){
      selected_attended = attendee_select.options[attendee_select.selectedIndex];
      attendee_id = selected_attended.value;
    }

    $.ajax({
      url: '/user/'+user_slug+'/slots.json',
      method: 'POST',
      dataType: 'json',
      data: {
        slot: {
          date_start: window.start,
          date_end: window.end,
          user: $("h1[data-slug]").data("slug"),
          event_type_id: event_type_id,
          attendee_id: attendee_id
        }
      },
      success: (data, status, xhr) =>{
        if(status=='success'){
          var options_time = {
            hour:'2-digit',
            minute:'2-digit'
          };
          let start_display = new Intl.DateTimeFormat("fr", options_time).format(Date.parse(start))
          let mstart = Date.parse(start)
          let mend = Date.parse(end)

          calendar.addEvent({
            start: start,
            end: end,
            title: data.title,
            id: data.id,
            event_type: event_type_label,
            event_color: data.event_color,
            extendedProps:{
              has_attendee: attendee_id,
              attendee_id: attendee_id
            }
          });

          $('#rdv-modal').modal('hide');
          creating = false;
        }
      }
    });
    creating = true;
  }
}

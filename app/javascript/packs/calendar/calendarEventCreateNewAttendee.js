// calendarEventCreateNewAttendee

document.addEventListener('turbolinks:load', () => {

  let form_attendee = document.getElementById('new_attendee_for_event');

  if(form_attendee != null){
    form_attendee.addEventListener('ajax:success', (event) => {
      let detail = event.detail;
      let data = detail[0];
      // let status = detail[1];
      let xhr = detail[2];
      let json = JSON.parse(data);

      // Add element and select it.
      let attendee_select = document.getElementById('attendee_id');
      let new_attendee = document.createElement('option');
      new_attendee.text = json.attendee.name;
      new_attendee.value = json.attendee.id;
      new_attendee.selected = 'selected';
      attendee_select.options.add(new_attendee);

      // replace form by saying the attendee is created.
      let card_body = form_attendee.parentNode;
      form_attendee.remove();

      let confirm_div = document.createElement('div');
      confirm_div.classList = 'fas fa-check fa-3x text-success';
      card_body.appendChild(confirm_div);

      let card_header = card_body.parentNode.querySelector('.card-header');
      card_header.innerText = new_attendee.text;
    });

    // Errors
    form_attendee.addEventListener('ajax:error', (event) => {

    });
  }

});

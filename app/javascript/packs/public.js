// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.

// require("@rails/ujs").start()
// require("turbolinks").start()
// require("@rails/activestorage").start()
// require("channels")

// Uncomment to copy all static images under ../images to the output folder and reference
// them with the image_pack_tag helper in views (e.g <%= image_pack_tag 'rails.png' %>)
// or the `imagePath` JavaScript helper below.
//
const images = require.context('../images', true)
const imagePath = (name) => images(name, true)

// require("trix")
// require("@rails/actiontext")

// import moment from 'moment';
// window.moment = moment;
// import 'jquery';
// import 'popper.js';
// import 'bootstrap/dist/js/bootstrap';

import '../stylesheets/public';

// import '@fortawesome/fontawesome-free/css/all';
import '@fortawesome/fontawesome-free/css/fontawesome.min';
import '@fortawesome/fontawesome-free/css/solid.min';
import '@fortawesome/fontawesome-free/css/brands.min';

import 'van11y-accessible-modal-window-aria';
import 'van11y-accessible-carrousel-aria';

require('./general');
require('./servers');

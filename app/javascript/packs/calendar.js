import { Calendar } from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import listPlugin from '@fullcalendar/list';
import interactionPlugin from '@fullcalendar/interaction';
import frLocale from '@fullcalendar/core/locales/fr';

import calendarEventCreate from './calendar/calendarEventCreate';
import calendarEventUpdate from './calendar/calendarEventUpdate';
import { ajax, darker } from './components/tools';

// const locale = 'fr';
let dtf = new Intl.DateTimeFormat(['fr', 'en']);
let rtf = new Intl.RelativeTimeFormat('fr-FR', { style: 'short' });
// let options = {
//    dateStyle: 'short',
//    timeStyle: 'full',
//    hour12: true,
//    day: 'numeric',
//    month: 'long',
//    year: '2-digit',
//    minute: '2-digit',
//    second: '2-digit',
// }
let options_time = {
  hour:'2-digit',
  minute:'2-digit'
};
let options_day = {
  weekday: 'long',
  day: 'numeric',
  month: 'long',
  year: 'numeric'
};
// en-GB
let calendar = null;
let title = "";
let start = "";
let end = "";
let day = "";
let prev_event = null;
let prev_popup = null;
let eventElement = null;
let passive_color = '#424242';
let active_color = '#1768E8';
let reserved_color = '#42BB42';
let passive_text = '#fff';
let reserved_text = '#000';
let context = null;
let context_name = null;
let context_desc = null;
// moment.locale(locale);
let creating = false;
let user = null;
let user_slug = null;
let today_btn = null;

let close_prev_popup = () =>{
  $(prev_popup).popover('dispose');
  prev_popup = null;
}

let initCalendar = () =>{
  let calendarEl = document.getElementById('calendar');
  if(calendarEl != undefined && calendarEl != null){

    context = document.getElementById('context');
    let events_url = '/user/'+user_slug+'/events';
    let hour_start = user.getAttribute('data-working-hour-start');
    let hour_end = user.getAttribute('data-working-hour-end');
    let hiddenDays = calendarEl.getAttribute('data-hidden-days');
    let workingDaysCount = calendarEl.getAttribute('data-working-days-count');

    calendar = new Calendar(calendarEl, {
      // timeZone: 'UTC',
      locales: [ frLocale ],
      locale: 'fr',
      eventLimit: true,
      plugins: [ interactionPlugin, dayGridPlugin, timeGridPlugin, listPlugin ],
      allDaySlot: false,
      header: {
        left:   'title',
        center: (workingDaysCount > 0 ? 'timeGridWorkingDays,' : '')+'timeGridDay,timeGridWeek,dayGridMonth',
        right:  'today prev,next'
      },
      defaultView: (workingDaysCount > 0 ? 'timeGridWorkingDays' : 'timeGridWeek'),
      views: {
        timeGridWorkingDays: {
          type: 'timeGrid',
          dayCount: workingDaysCount,
          buttonText: 'Working days',
          hiddenDays: (hiddenDays.length < 7 ? hiddenDays : null )
        },
        timeGridWeek: {
          buttonText: '7 J'
        }
      },
      nowIndicator: true,
      minTime: hour_start+':00:00',
      maxTime: hour_end+':00:00',
      firstDay: 1,
      selectable: true,
      eventBackgroundColor: passive_color,
      eventTextColor: passive_text,
      displayEventTime: false,
      events: '/user/'+user_slug+'/events',
      editable: true,
      eventRender: (info) =>{
        let params = info.event.extendedProps;
        if(params.has_attendee){
          // info.el.style.backgroundColor = reserved_color;
          info.el.style.color = reserved_text;
        }
        if(params.event_color != null){
          info.el.style.backgroundColor = params.event_color;
        }
      },
      select: (info) => {
        // Select an empty slot to create one
        if(prev_popup != null){
          close_prev_popup();
        }else{
          title = "Nouveau rendez-vous";
          window.start = info.startStr;
          window.end = info.endStr;
          start = window.start;
          end = window.end;
          $('#rdv-modal').modal();
        }
      },
      eventMouseEnter: (mouseEnterInfo) => {
        let params = mouseEnterInfo.event.extendedProps;

        mouseEnterInfo.el.style.backgroundColor = darker(params.event_color);
        mouseEnterInfo.el.style.boxShadow = '1px 1px 3px '+params.event_color;

        if(params.attendee_id != null && params.attendee_id != ""){
          // let attendee_url = "/user/"+user_slug+"/attendees/"+params.attendee_id
          // ajax(attendee_url, (data)=>{
          //   if(context_name != null){
          //     context_name.remove();
          //   }
          //   if(context_desc != null){
          //     context_desc.remove();
          //   }
          //   let json = JSON.parse(data);
          //   context_name = document.createTextNode(json.name);
          //   context_desc = document.createElement('div');
          //   context_desc.innerHTML = json.description.body;
          //   context.appendChild(context_name);
          //   context.appendChild(context_desc);
          //   context.style = "display: block;"
          // });
        }
      },
      eventMouseLeave: (mouseLeaveInfo) =>{
        let params = mouseLeaveInfo.event.extendedProps;
        if(params.event_color != null){
            mouseLeaveInfo.el.style.backgroundColor = params.event_color;
        }else{
          if(params.has_attendee){
            mouseLeaveInfo.el.style.backgroundColor = reserved_color;
          }else{
            mouseLeaveInfo.el.style.backgroundColor = passive_color;
          }
        }
        mouseLeaveInfo.el.style.boxShadow = 'none';
        context.style = "display: none;"
      },
      eventClick: (info) =>  {
        if(prev_popup != null){
          close_prev_popup();
        }

        let params = info.event.extendedProps;
        let has_attendee = false;
        if(params.has_attendee){
          has_attendee = true;
        }

        // pop
        let event = info.event;

        if(has_attendee){
          let slot_url = "/user/"+user_slug+"/slots/"+info.event.id;
          title = $("<a href='"+slot_url+"' class='text-dark'>"+event.title+"</a>");
        }else{
          title = event.title;
        }
        eventElement = info.el;

        eventElement.setAttribute("data-toggle", "popover");
        let pop_content = null;

        let button_delete = $("<a class='btn btn-sm btn-danger' "
                            +"href='/user/"+user_slug+"/slots/"+event.id+"' data-method='delete' rel='nofollow' data-remote='true'>"
                            +"<span class='fas fa-trash'></span> Delete</a>");
        button_delete[0].addEventListener('ajax:success', (data) =>{
          close_prev_popup();
          event.remove();
        });

        // TODO: on cancel change event_type ?!
        let button_cancel = $("<a class='btn btn-sm btn-warning' "
                            +"href='/user/"+user_slug+"/slots/"+event.id+"/cancel' data-method='put' rel='nofollow'>"
                            +"<span class='fas fa-eraser'></span>Cancel</a>");

        let button_patient = $("<a href='/user/"+user_slug+"/slots/"+event.id+"/edit' class='btn btn-sm btn-info'>"
                              +"Prendre RDV</a>");

        let start_time = new Intl.DateTimeFormat("fr", options_time).format(Date.parse(event.start))
        let end_time = new Intl.DateTimeFormat("fr", options_time).format(Date.parse(event.end))

        pop_content = $("<div class='event_popup'>"+start_time+" - "+end_time+"<br></div>");
        let dom_event_type = $("<small>"+params.event_type+"</smalL><br/>");
        pop_content.prepend(dom_event_type);

        if(has_attendee){
          pop_content.append(button_cancel);
        }else{
          if(params.event_type=='availability'){
            pop_content.append(button_patient);
          }
          pop_content.append(button_delete);
        }

        prev_popup = $(eventElement).popover({
          title: title,
          content: pop_content,
          html: true
        });
        $(eventElement).popover('show');
        $('.popover').css({'border-color': params.event_color});

      },
      eventResize: (eventResizeInfo) =>{
        close_prev_popup();
        calendarEventUpdate(eventResizeInfo);
      },
      eventDrop: (eventDropInfo) =>{
        close_prev_popup();
        calendarEventUpdate(eventDropInfo);
      }
    });

    calendar.render();
    today_btn = document.querySelector('.fc-timeGridDay-button');
    if (window.matchMedia("(max-width: 768px)").matches) {
      today_btn.click();
    }
  }
}

let eventTypeChoiceHandler = (ev) =>{
  let event_type_select = document.getElementById('event_type');
  let selected_option = event_type_select.options[event_type_select.selectedIndex];
  let attendee_select = document.getElementById('attendee_id');
  if(!['availability', 'offtime'].includes(selected_option.getAttribute('data-type'))){
    attendee_select.style = '';
    attendee_select.parentNode.style = '';
    document.querySelector("#rdv-modal .modal-dialog").classList = "modal-dialog modal-lg";
  }else{
    attendee_select.style = 'display:none;';
    attendee_select.parentNode.style = 'display:none;';
    document.querySelector("#rdv-modal .modal-dialog").classList = "modal-dialog modal-sm";
  }
}

document.addEventListener('turbolinks:load', () => {

  user = document.getElementById('user');
  if(user != null){
    user_slug = user.getAttribute('data-slug');
    initCalendar();
  }

  // let modal_rdv = document.getElementById('rdv-modal');
  $('#rdv-modal').on('shown.bs.modal', function (e) {
    let day = new Intl.DateTimeFormat("fr", options_day).format(Date.parse(start))
    let start_display = new Intl.DateTimeFormat("fr", options_time).format(Date.parse(start))
    let end_display = new Intl.DateTimeFormat("fr", options_time).format(Date.parse(end))

    $("#rdv-modal .modal-body .start, #rdv-modal .modal-body .end").empty();
    $("#rdv-modal .modal-title").text(title);

    let day_dom = $("<strong>"+day+"</strong><br>");
    let end_dom = $("<p>"+start_display+" - "+end_display+"</p><br>");

    $("#rdv-modal .modal-body .start").append(day_dom);
    $("#rdv-modal .modal-body .end").append(end_dom);

    $("#save-slot").on('click', (ev) =>{
      ev.stopPropagation();
      calendarEventCreate(calendar);
    });
    eventTypeChoiceHandler();

  });
  // $('#rdv-modal').on('hidden.bs.modal', (e) =>{
  //   // reset modal view
  // });

  let event_type_select = document.getElementById('event_type');
  if(event_type_select != null){
    event_type_select.addEventListener('change', eventTypeChoiceHandler);
  }

  window.addEventListener('resize', () => {
    if (window.matchMedia("(max-width: 768px)").matches) {
      if(today_btn != null){
        today_btn.click();
      }
    }
  }, false);

});

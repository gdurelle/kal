let removeNode = (node) =>{
  node.remove()
}

document.addEventListener('turbolinks:load', () => {

  // Flashes
  setTimeout( ()=> {
    let alerts = document.querySelectorAll('.alert.alert-success');
    for (let i = alerts.length - 1; i >= 0; i--) {
      if(alerts[i].style.opacity == ""){
        alerts[i].style.opacity = 1;
      }
      opacity_int = parseInt(alerts[i].style.opacity);
      let opa_fade = setInterval( () => {
        opacity_int = opacity_int - 0.1;
        alerts[i].style.opacity = opacity_int;
        if(opacity_int <= 0){
          removeNode(alerts[i]);
          clearInterval(opa_fade);
        }
      }, 100);
    }
  }, 5000);

});

// document.addEventListener("DOMContentLoaded", function() {
//   let modal_agenda = document.getElementById('modal-agenda');
//   let carousel_agenda = document.getElementById('modal-carousel-agenda');
//   if(modal_agenda){
//     van11yAccessibleModalWindowAria(modal_agenda);
//   }
//   if(carousel_agenda){
//     van11yAccessibleCarrouselAria(carousel_agenda);
//   }
// });

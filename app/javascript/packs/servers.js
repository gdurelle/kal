const schema = "https://";
let servers = null;

let kalUsersFor = (server) => {
  $.ajax({
    url: '/servers/'+server.id+'.json',
    method: 'GET',
    dataType: 'json',
    success: (data, status, xhr) =>{
      if(status=='success'){
        if(data.users.length > 0){
          let other_users = $("#other_users");
          let title = $("<h2>"+other_users.data('title')+"</h2>");
          other_users.before(title);
        }
        for(let j = 0; j < data.users.length ; j++){
          displayUser(data.users[j], data.server);
        }
      }
    }
  });
};

let displayUser = (user, server) =>{
  let user_dom = $("<div class='col mb-1'></div>");
  let user_dom_card = $("<div class='card bg-blue'></div>");
  let user_dom_header = $("<div class='card-header'></div>");
  user_dom.append(user_dom_card);
  user_dom_card.append(user_dom_header);
  let name = user.firstname+" "+user.lastname;
  let url = server.url+':'+server.port+'/user/'+user.slug;
  let link = $("<a href="+schema+url+">"+name+"</a>");
  user_dom_header.append(link);
  $("#other_users").append(user_dom);
}

document.addEventListener('turbolinks:load', () => {
  let other_users = document.getElementById("other_users");
  if(servers == null && other_users != null){
    $.ajax({
      url: '/servers.json',
      method: 'GET',
      dataType: 'json',
      success: (data, status, xhr) =>{
        if(status=='success'){
          servers = data;
          for(let i = 0; i < servers.length; i++){
            kalUsersFor(servers[i]);
          }
        }
      }
    });
  }
});

class HomeController < ApplicationController
  skip_before_action :authenticate_user!
  skip_authorization_check

  layout 'public'

  def index
    @users = User.last(2)
    @settings = Admin::Setting.first
    @home_disclaimer = @settings.home_disclaimer
  end

  def privacy
    @privacy = Admin::Setting.first.privacy
  end

  def legal
    @legal = Admin::Setting.first.legal
  end

  def tos
    @tos = Admin::Setting.first.tos
  end

  def credits; end

  def security; end
end

class SlotsController < ApplicationController
  skip_before_action :authenticate_user!, only: %i[book booking]
  before_action :authenticate_attendee!, only: %i[book]
  before_action :authenticate_attendee!, only: %i[cancel], unless: -> { user_signed_in? }
  load_and_authorize_resource

  # GET /slots/1
  # GET /slots/1.json
  def show; end

  # GET /slots/new
  def new
    @attendees = current_user.attendees
    @attendee = current_user.attendees.new
    @start = DateTime.parse(params[:date_start]) if params[:date_start]
    @end = DateTime.parse(params[:date_end]) if params[:date_end]
    @slot = @attendee.slots.build
    @slot.user = current_user
  end

  # GET /slots/1/edit
  def edit
    @attendees = current_user.attendees
  end

  # GET /slots/id/book
  def book; end

  # POST /slots/id/booking
  def booking
    respond_to do |format|
      if @slot.update(slot_params)
        format.html do
          if attendee_signed_in?
            @slot.user.attendees << current_attendee unless @slot.user.attendees.include?(current_attendee)
            redirect_to prepare_slot_path(@slot.user.slug, @slot), notice: I18n.t(:slot_booked, scope: %i[flashes slots booking])
          else
            redirect_to new_attendee_registration_path(slot: @slot.id), alert: I18n.t(:subscribe_first, scope: %i[flashes slots booking])
          end
        end
        format.json { render json: { status: :ok } }
      else
        format.html { render :book }
        format.json { render json: @slot.errors, status: :unprocessable_entity }
      end
    end
  end

  # POST /slots
  # POST /slots.json
  def create
    @slot = current_user.slots.new(slot_params)
    respond_to do |format|
      if @slot.save
        format.html { redirect_to slot_path(@slot.user.slug, @slot), notice: 'Slot was successfully created.' }
        format.json { render json: @slot, status: :created }
      else
        format.html do
          @attendees = current_user.attendees
          render :new
        end
        format.json { render json: @slot.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /slots/1
  # PATCH/PUT /slots/1.json
  def update
    respond_to do |format|
      if @slot.update(slot_params)
        format.html do
          if attendee_signed_in?
            @slot.user.attendees << current_attendee unless @slot.user.attendees.include?(current_attendee)
            redirect_to attendee_agenda_path, notice: 'Slot was successfully updated.'
          else
            @slot.user.attendees << @slot.attendee unless @slot.user.attendees.include?(@slot.attendee)
            redirect_to slot_path(@slot.user.slug, @slot), notice: 'Slot was successfully updated.'
          end
        end
        format.json { render json: { status: :ok } }
      else
        @attendees = current_user.attendees
        format.html { render :edit }
        format.json { render json: @slot.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /slots/1
  # DELETE /slots/1.json
  def destroy
    @slot.destroy
    respond_to do |format|
      format.html { redirect_to root_url, notice: 'Slot was successfully destroyed.' }
      format.json { head :no_content }
      format.js { head :no_content }
    end
  end

  # PUT /slots/1/cancel
  # PUT /slots/1/cancel.json
  def cancel
    if attendee_signed_in?
      respond_to do |format|
        if @slot.cancel_by_attendee
          format.html do
            redirect_to attendee_agenda_path, notice: 'Slot was successfully canceled.'
          end
          format.json { head :no_content }
        else
          format.html do
            redirect_to attendee_agenda_path, alert: @slot.errors.full_messages.join(', ')
          end
          format.json { render json: @slot.errors, status: :unprocessable_entity }
        end
      end
    else
      @slot.update_attribute :attendee_id, nil
      respond_to do |format|
        format.html do
          redirect_to user_agenda_path(current_user), notice: 'Slot was successfully canceled.'
        end
        format.json { head :no_content }
      end
    end
  end

  private

  # Never trust parameters from the scary internet, only allow the white list through.
  def slot_params
    params.require(:slot).permit :date_start, :date_end, :user, :attendee_id, :event_type_id,
                                 :notes, :on_time, :behaviour
  end

  def current_ability
    @current_ability ||= AbilityAttendee.new if attendee_signed_in?
    @current_ability ||= Ability.new(current_user)
  end
end

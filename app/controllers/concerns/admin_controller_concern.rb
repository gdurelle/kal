module AdminControllerConcern
  extend ActiveSupport::Concern

  included do
    before_action :authenticate_admin!, unless: -> { devise_controller? }
  end

  def current_ability
    @current_ability ||= AbilityAdmin.new
  end
end

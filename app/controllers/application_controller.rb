class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception, prepend: true
  before_action :authenticate_user!, unless: -> { devise_controller? || admin_signed_in? || attendee_signed_in? }
  check_authorization unless: :devise_controller?

  before_action :custom_settings
  before_action :configure_permitted_parameters, if: :devise_controller?
  layout :for_connected_entity
  around_action :switch_locale

  rescue_from CanCan::AccessDenied do |exception|
    respond_to do |format|
      format.json { head :forbidden, content_type: 'text/html' }
      format.html do
        redirect_to main_app.root_url, alert: exception.message
      end
      format.js { head :forbidden, content_type: 'text/html' }
    end
  end

  # Devise
  def after_sign_in_path_for(resource)
    if resource.class.to_s == 'Admin'
      admin_dashboard_path
    elsif resource.class.to_s == 'User'
      user_agenda_path(resource.slug)
    elsif resource.class.to_s == 'Attendee'
      attendee_agenda_path
    else
      root_path
    end
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit :sign_up, keys: %i[firstname lastname]
    devise_parameter_sanitizer.permit :account_update, keys: %i[firstname lastname].concat(work_params)
  end

  private

  def custom_settings
    @settings = Admin::Setting.first
    @meta_title = @settings.title
    @meta_description = @settings.description
    @meta_twitter_site = @settings.twitter_site
    @meta_twitter_creator = @settings.twitter_creator
  end

  # Layout
  def for_connected_entity
    if signed_in? && current_attendee.present?
      'attendee'
    elsif devise_controller? && (new_session? || new_registration? || new_password?)
      'public'
    else
      'application'
    end
  end

  def new_registration?
    controller_name == 'registrations' && action_name == 'new'
  end

  def new_password?
    controller_name == 'passwords' && action_name == 'new'
  end

  def new_session?
    controller_name == 'sessions'
  end

  # params for Devise update
  def work_params
    [
      :notes, :default_slot_duration, :description,
      :generate_slots, :public_slots,
      :default_start_time, :default_end_time,
      :cancel_delay, :cancel_reopen, :image,
      working_days: [], working_hours: []
    ]
  end

  def switch_locale(&action)
    locale = extract_locale_from_accept_language_header || I18n.default_locale
    locale = params[:locale] unless params[:locale].blank?
    I18n.with_locale(locale, &action)
  end

  def extract_locale_from_accept_language_header
    request.env['HTTP_ACCEPT_LANGUAGE'].scan(/^[a-z]{2}/).first if request.env['HTTP_ACCEPT_LANGUAGE'].present?
  end
end

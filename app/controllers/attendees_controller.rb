class AttendeesController < ApplicationController
  skip_before_action :authenticate_user!
  before_action :authenticate_attendee!
  # authorize_resource :agenda
  skip_authorization_check

  def agenda
    @slots = current_attendee.slots.in_future.order('date_start DESC')
  end

  private

  def attendee_params
    params.require(:attendee).permit :email, :firstname, :lastname, :phone
  end

  def current_ability
    @current_ability ||= AbilityAttendee.new(current_attendee)
  end
end

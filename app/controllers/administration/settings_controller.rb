module Administration
  class SettingsController < ApplicationController
    include AdminControllerConcern
    authorize_resource class: Admin::Setting

    def update
      @admin_setting = Admin::Setting.first
      respond_to do |format|
        if @admin_setting.update(admin_setting_params)
          @admin_setting.image.attach(params[:image]) if params[:image].present?
          format.html { redirect_to admin_settings_path, notice: 'Settings successfully updated.' }
          format.json { head :ok }
        else
          format.json { render json: @admin_setting.errors, status: :unprocessable_entity }
        end
      end
    end

    def delete_image
      @admin_setting = Admin::Setting.first
      @admin_setting.image.purge_later
      respond_to do |format|
        format.html { redirect_to admin_settings_path, notice: 'Image has been marked for deletion.' }
      end
    end

    private

    def admin_setting_params
      params.require(:admin_setting).permit :register_pro, :max_users,
                                            :register_attendee,
                                            :home_disclaimer,
                                            :tos, :privacy, :legal,
                                            :title, :description, :image,
                                            :twitter_site, :twitter_creator
    end
  end
end

module Administration
  class UsersController < ApplicationController
    include AdminControllerConcern
    load_and_authorize_resource

    # GET /administration/users
    # GET /administration/users.json
    def index
      @users = User.all.order('updated_at DESC')
    end

    # GET /administration/users/new
    def new; end

    # GET /administration/users/1/edit
    def edit; end

    # POST /administration/users
    # POST /administration/users.json
    def create
      temporary_generated_password = Devise.friendly_token.first(8)
      @user.password = temporary_generated_password

      respond_to do |format|
        if @user.valid?
          User.transaction do
            @user.save
            RegistrationMailer.welcome(@user, temporary_generated_password).deliver_later
          end
          format.html { redirect_to admin_users_path, notice: 'User was successfully created.' }
          format.json { render :show, status: :created, location: @user }
        else
          format.html { render :new }
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /administration/users/1
    # PATCH/PUT /administration/users/1.json
    def update
      respond_to do |format|
        if @user.update(user_params)
          format.html { redirect_to admin_users_path, notice: 'User was successfully updated.' }
          format.json { render :show, status: :ok, location: @user }
        else
          format.html { render :edit }
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /administration/users/1
    # DELETE /administration/users/1.json
    def destroy
      @user.destroy
      respond_to do |format|
        format.html { redirect_to admin_users_url, notice: 'User was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit :firstname, :lastname, :email
    end
  end
end

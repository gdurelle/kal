module Administration
  class DashboardController < ApplicationController
    include AdminControllerConcern
    authorize_resource class: false

    before_action :load_admin_settings, only: %i[index settings]

    def index
      @servers_count = Server.count
    end

    def settings; end

    private

    def load_admin_settings
      @admin_settings = ::Admin::Setting.first_or_create
      @users_count = User.count
    end
  end
end

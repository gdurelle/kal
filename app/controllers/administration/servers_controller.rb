module Administration
  class ServersController < ApplicationController
    include AdminControllerConcern
    load_and_authorize_resource

    # GET /servers
    # GET /servers.json
    def index
      # @servers = Server.all
    end

    # GET /servers/1
    # GET /servers/1.json
    def show; end

    # GET /servers/new
    def new
      # @server = Server.new
    end

    # GET /servers/1/edit
    def edit; end

    # POST /servers
    # POST /servers.json
    def create
      # @server = Server.new(server_params)

      respond_to do |format|
        if @server.save
          format.html { redirect_to admin_servers_path, notice: 'Server was successfully created.' }
          format.json { render :show, status: :created, location: @server }
        else
          format.html { render :new }
          format.json { render json: @server.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /servers/1
    # PATCH/PUT /servers/1.json
    def update
      respond_to do |format|
        if @server.update(server_params)
          format.html { redirect_to admin_servers_path, notice: 'Server was successfully updated.' }
          format.json { render :show, status: :ok, location: @server }
        else
          format.html { render :edit }
          format.json { render json: @server.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /servers/1
    # DELETE /servers/1.json
    def destroy
      @server.destroy
      respond_to do |format|
        format.html { redirect_to admin_servers_url, notice: 'Server was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private

    # Never trust parameters from the scary internet, only allow the white list through.
    def server_params
      params.require(:server).permit :url, :port, :sync_from, :sync_to
    end
  end
end

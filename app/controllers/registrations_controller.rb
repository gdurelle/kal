# app/controllers/registrations_controller.rb
class RegistrationsController < Devise::RegistrationsController
  before_action :check_settings, only: :new
  prepend_before_action :authenticate_scope!, only: %i[edit update destroy delete_image]
  before_action :check_dummy_robot, only: :create

  def edit
    @event_types = EventType.all
  end

  def new
    @slot = Slot.find(params[:slot]) if params[:slot].present? && params[:slot].to_i.is_a?(Numeric)
    super
  end

  def create
    super do |resource|
      if resource.valid? && params[:slot].present? && params[:slot].to_i.is_a?(Numeric)
        @slot = Slot.find(params[:slot])
        @slot.attendee = resource
        @slot.user.attendees << resource unless @slot.user.attendees.include?(resource)
        @slot.save
      end
    end
  end

  def delete_image
    resource.image.purge_later
    redirect_to edit_registration_path(resource, anchor: 'profile')
  end

  protected

  def update_resource(resource, params)
    params['working_days'] = params['working_days'].to_set.delete('0').map(&:to_i) if params['working_days'].present?
    resource.update_without_password(params)
  end

  def after_update_path_for(resource)
    edit_registration_path(resource)
  end

  def after_sign_up_path(resource)
    edit_registration_path(resource)
  end

  def check_settings
    settings = Admin::Setting.first
    if resource_name == :user && settings.register_pro?
      return if settings.max_users.to_i.zero? || settings.max_users > User.count
    end
    return if resource_name == :attendee && settings.register_attendee?

    redirect_to root_path, alert: I18n.t(:registrations_are_closed, scope: :admin)
  end

  def check_dummy_robot
    redirect_to root_path, alert: I18n.t(:registrations_are_closed, scope: :admin) if params[:dummy_robot].present?
  end
end

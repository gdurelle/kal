class UserAttendeesController < ApplicationController
  load_and_authorize_resource through: :current_user

  # GET /user_attendees
  # GET /user_attendees.json
  def index
    @user_attendees = current_user.user_attendees.includes(:attendee)
  end

  # GET /user_attendees/1
  # GET /user_attendees/1.json
  def show
    @attendee = @user_attendee.attendee
    @meetings = @attendee.slots.where(user: current_user).order('date_start DESC')
    @on_time = @attendee.on_time
    @behaviour = @attendee.behaviour
    respond_to do |format|
      format.json do
        render json: @attendee.as_json
      end
      format.html do
        render :show
      end
    end
  end

  # GET /user_attendees/new
  def new
    @attendee = Attendee.new
    @user_attendee = current_user.user_attendees.new(attendee: @attendee)
  end

  # GET /user_attendees/1/edit
  def edit
    @attendee = @user_attendee.attendee
  end

  # POST /user_attendees
  # POST /user_attendees.json
  def create
    @attendee = Attendee.new(attendee_params)
    respond_to do |format|
      if @attendee.valid? && @attendee.save
        current_user.attendees << @attendee
        format.html { redirect_to @user_attendee, notice: 'User attendee was successfully created.' }
        format.js do
          render json: { attendee: @attendee.as_json, status: :created }.to_json
        end
      else
        format.html { render :new }
        format.json { render json: @user_attendee.errors.merge(@attendee.errors).full_messages.join(', '), status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /user_attendees/1
  # PATCH/PUT /user_attendees/1.json
  def update
    respond_to do |format|
      if @user_attendee.attendee.update(attendee_params)
        format.html { redirect_to @user_attendee, notice: 'User attendee was successfully updated.' }
        format.json { render :show, status: :ok, location: @user_attendee }
      else
        @attendee = @user_attendee.attendee
        format.html { render :edit }
        format.json { render json: @user_attendee.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /user_attendees/1
  # DELETE /user_attendees/1.json
  def destroy
    @attendee = @user_attendee.attendee
    @user_attendee.destroy
    if @attendee.encrypted_password.blank? && @attendee.users.blank?
      @attendee.destroy
    end
    respond_to do |format|
      format.html { redirect_to user_attendees_url, notice: 'Attendee relation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  def attendee_params
    params.require(:attendee).permit :email, :firstname, :lastname, :phone
  end
end

class ServersController < ApplicationController
  skip_before_action :authenticate_user!
  load_resource
  skip_authorization_check

  def index
    respond_to do |format|
      format.json do
        # @servers = Server.all
        render json: @servers.as_json
      end
    end
  end

  # Display users of this server
  def show
    respond_to do |format|
      format.json do
        @server = Server.find(params[:id])
        url = @server.url + ':' + @server.port.to_s + '/users.json'
        response = HTTParty.get(url, format: :plain)
        json = JSON.parse(response.body, symbolize_names: true) if response.body.present?
        if json
          render json: { users: json, server: { url: @server.url, port: @server.port } }
        else
          Rails.logger.debug { '---' + response.code.to_s }
          Rails.logger.debug { '---' + response.message }
          Rails.logger.debug { '---' + response.body }
          Rails.logger.debug { '---' + response.headers.inspect }
        end
      end
    end
  end
end

class UsersController < ApplicationController
  skip_before_action :authenticate_user!, only: %i[profile index]
  skip_authorization_check only: %i[profile index]
  load_and_authorize_resource

  def profile
    @user = User.friendly.find(params[:user_slug])
    @available_slots = @user.slots.free.in_future.order('date_start asc').first(6)
    @working_days = @user.working_days.to_set
    render layout: 'public'
  end

  def agenda
    raise CanCan::AccessDenied.new(nil, :agenda, User) if params[:user_slug] != current_user.slug

    days_indexes = I18n.t('date.day_names').map { |dn| I18n.t('date.day_names').find_index(dn) }
    @hidden_days_indexes = days_indexes - current_user.working_days.to_a
    @event_types = EventType.all
    @attendees = current_user.attendees
  end

  def events
    @user = User.friendly.find(params[:user_slug])
    @slots = @user.slots
    respond_to do |format|
      format.json do
        render json: @slots.as_json
      end
    end
  end

  # Used by server connection of other servers
  def index
    @users = User.last(8)
    respond_to do |format|
      format.json do
        render json: @users.as_json
      end
    end
  end
end

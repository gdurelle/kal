module EventTypesHelper
  def dynamic_translation(*args)
    param = args[0]

    if param.class == EventType
      EventType::ALL.include?(param.label) ? I18n.t(param.label, scope: :event_types) : param.label
    else
      dynamic_translation_for_array(*args)
    end
  end

  def event_type_data_type(event_type)
    if event_type.is_meeting
      'meeting'
    elsif event_type.is_offtime
      'offtime'
    else
      'availability'
    end
  end

  private

  def dynamic_translation_for_array(event_types)
    event_types.map do |evt|
      [
        EventType::ALL.include?(evt.label) ? I18n.t(evt.label, scope: :event_types) : evt.label,
        evt.id
      ]
    end
  end
end

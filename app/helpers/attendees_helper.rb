module AttendeesHelper
  def display_on_time(label)
    case label
    when 'ok'
      content_tag :span, nil, class: 'fas fa-smile fa-2x text-success'
    when 'neutral'
      content_tag :span, nil, class: 'fas fa-meh fa-2x blue_light'
    when 'bad'
      content_tag :span, nil, class: 'fas fa-frown fa-2x yellow'
    when 'danger'
      content_tag :span, nil, class: 'fas fa-frown fa-2x red'
    end
  end
end

module ApplicationHelper
  # apply the good twitter class for the type.
  def twitter_alert_type(type)
    case type
    when 'error'
      'alert-danger'
    when 'alert', 'warning'
      'alert-warning'
    when 'info', 'infos'
      'alert-info'
    when 'notice', 'success'
      'alert-success'
    end
  end
end

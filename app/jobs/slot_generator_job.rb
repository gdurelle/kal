class SlotGeneratorJob < ApplicationJob
  queue_as :default

  # SlotGeneratorJob.perform_now(User.last.id)
  # SlotGeneratorJob.perform_now(User.first.id)
  # SlotGeneratorJob.perform_later(User.last.id)
  def perform(user_id)
    user = User.find(user_id)
    return unless user.generate_slots?

    I18n.locale = 'en'
    duration = user.default_slot_duration
    working_hours = user.working_hours
    workdays = user.working_days.map { |work_day_n| I18n.t('date.day_names').map(&:downcase)[work_day_n] }.compact
    BusinessTime::Config.work_week = workdays.map { |day_name| day_name[0..2] }
    event_type = EventType.find_by(label: EventType::AVAILABILITY)

    (Date.today..Date.today.advance(weeks: 2)).each do |day|
      next unless day.workday?

      first_hour = day.to_datetime.advance(hours: working_hours.first.to_i)
      last_hour = day.to_datetime.advance(hours: working_hours.last.to_i)
      # loop hours
      (first_hour.to_i..last_hour.to_i).step(duration.minutes) do |hour|
        next unless working_hours.include?(DateTime.strptime(hour.to_s, '%s').hour.to_s)

        Slot.find_or_create_by!(
          date_start: DateTime.strptime(hour.to_s, '%s'),
          date_end: DateTime.strptime(hour.to_s, '%s').advance(minutes: user.default_slot_duration),
          user_id: user_id
        ) do |new_slot|
          new_slot.event_type = event_type
        end
      end
    end
    # End of prerform
  end
end

class AttendeeBehaviourLoopJob < ApplicationJob
  queue_as :low_priority

  def perform
    attendees_to_loop = Attendee.select(:id)
                                .left_joins(:slots)
                                .group(:id)
                                .having("COUNT(slots.id) > 0
                                        AND (behaviour_updated_at
                                        IS NULL OR behaviour_updated_at < ?)", DateTime.now.advance(hours: -48))

    attendees_to_loop.each do |attendee|
      AttendeeBehaviourCalculusJob.perform_later(attendee.id)
    end
  end
end

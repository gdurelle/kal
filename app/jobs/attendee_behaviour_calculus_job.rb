class AttendeeBehaviourCalculusJob < ApplicationJob
  queue_as :low_priority

  def perform(attendee_id)
    @attendee = Attendee.find(attendee_id)

    @meetings = @attendee.slots
                         .where('date_start < ?', DateTime.now)
                         .order('date_start DESC')

    on_time = @meetings.pluck(:on_time)
    on_time_label = on_time.max_by { |i| on_time.count(i) }

    behaviour = @meetings.pluck(:behaviour)
    behaviour_label = behaviour.max_by { |i| behaviour.count(i) }

    # update attendee
    @attendee.update_attributes on_time: on_time_label,
                                behaviour: behaviour_label,
                                behaviour_updated_at: DateTime.now
  end
end

class UserSlotLoopJob < ApplicationJob
  queue_as :default

  def perform
    users_to_loop = User.where(generate_slots: true)
                        .where('working_days IS NOT NULL
                               AND json_array_length(working_days) > 0')
    users_to_loop.each do |user|
      SlotGeneratorJob.perform_later(user.id)
    end
  end
end

class Attendee < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_many :slots, dependent: :destroy
  accepts_nested_attributes_for :slots

  has_many :user_attendees
  has_many :users, through: :user_attendees

  has_rich_text :description

  validates :email, uniqueness: false, allow_nil: true
  validates :firstname, :lastname, presence: true

  def name
    [firstname, lastname].join(' ')
  end

  def slots_with(user)
    slots.where(user: user).count
  end

  def as_json
    {
      id: id,
      name: name,
      phone: phone
    }
  end

  protected

  def password_required?
    false
  end

  def email_required?
    false
  end
end

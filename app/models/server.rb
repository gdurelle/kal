class Server < ApplicationRecord
  validates :url, presence: true

  def as_json
    {
      id: id
    }
  end
end

# rubocop:disable Style/ClassAndModuleChildren
class Admin::Setting < ApplicationRecord
  include AdminConcern
  has_rich_text :home_disclaimer
  has_rich_text :tos
  has_rich_text :privacy
  has_rich_text :legal
  has_one_attached :image
end
# rubocop:enable Style/ClassAndModuleChildren

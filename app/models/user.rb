class User < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: :slugged

  DAYS = %w[Su Mo Tu We Th Fr Sa].freeze

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_rich_text :notes
  has_rich_text :description

  has_many :slots, dependent: :destroy
  has_many :user_attendees, dependent: :destroy
  has_many :attendees, through: :user_attendees

  validates :default_slot_duration, presence: true

  after_update :generate_new_slots
  has_one_attached :image

  def name
    [firstname, lastname].join(' ')
  end

  private

  def should_generate_new_friendly_id?
    slug.blank? || (firstname_changed? || lastname_changed?)
  end

  def generate_new_slots
    SlotGeneratorJob.perform_later(id) if generate_slots?
  end
end

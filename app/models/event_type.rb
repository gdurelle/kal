class EventType < ApplicationRecord
  AVAILABILITY = 'availability'.freeze
  MEETING = 'meeting'.freeze
  OFFTIME = 'offtime'.freeze
  ALL = [EventType::AVAILABILITY, EventType::MEETING, EventType::OFFTIME].freeze

  has_many :slots, dependent: :destroy

  validates :label, presence: true
end

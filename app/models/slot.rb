require 'action_view'

class Slot < ApplicationRecord
  include ActionView::Helpers::DateHelper

  BEHAVE_NEUTRAL = 'neutral'.freeze

  belongs_to :user
  belongs_to :attendee, required: false
  belongs_to :event_type, required: true

  validates :date_start, :date_end, presence: true

  scope :free, -> { where(attendee_id: nil).joins(:event_type).where(event_types: { is_available: true }) }
  scope :taken, -> { where('attendee_id IS NOT NULL') }
  scope :in_future, -> { where('date_start > ?', DateTime.now) }

  has_rich_text :notes

  def title(is_attendee = false)
    duration = distance_of_time_in_words(date_end, date_start)
    if attendee.blank?
      "#{date_start.strftime('%H:%M')} - #{duration}"
    else
      is_attendee = is_attendee.blank? ? false : is_attendee[:is_attendee]
      is_attendee ? user.lastname : "#{attendee.name} - #{duration}"
    end
  end

  def as_json(is_attendee = false)
    {
      id: id,
      title: title(is_attendee),
      start: date_start,
      end: date_end,
      user: user.name,
      event_type: event_type&.label,
      event_color: event_type&.color,
      extendedProps: {
        has_attendee: !attendee.blank?,
        attendee_id: (attendee.blank? ? '' : attendee.id)
      }
    }
  end

  def user_attendee
    attendee.user_attendees.where(user: user).first
  end

  def cancel_by_attendee
    return update_attributes(attendee_id: nil) if Time.now < date_start.advance(hours: -user.cancel_delay)

    errors.add :base, :not_in_cancel_delay, { delay: "#{user.cancel_delay}H" }
    destroy if event_type.is_available && !user.cancel_reopen
  end
end

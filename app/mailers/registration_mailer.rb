class RegistrationMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.registration_mailer.welcome.subject
  #
  def welcome(user, temporary_generated_password)
    @greeting = "Hi"
    @user = user
    @temporary_generated_password = temporary_generated_password

    mail to: user.email
  end
end

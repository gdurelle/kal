# KAL

* Ruby 3
* Rails 6

## Dependencies
  * PostgreSQL
  * Redis

## Setup

See the [Wiki](https://gitlab.com/gdurelle/kal/-/wikis/Home)

* Services (job queues, cache servers, search engines, etc.)

# Deployment

## Staging
```cap staging deploy```

# INSTALL on new server

## Metal

```
APT_LISTCHANGES_FRONTEND=none apt-get update -y
APT_LISTCHANGES_FRONTEND=none apt-get update --fix-missing
APT_LISTCHANGES_FRONTEND=none apt-get upgrade -y
```

ssh key needed on PI
public key on gitlab config
```
sudo -s
mkdir -p /srv/www
chown pi /srv/www
```
vim database.yml

### user
```
adduser kal
usermod -a -G root,adm,sudo kal
```
### Ruby env
_adapt with latest version of Ruby_

#### Ubuntu dependencies
From https://github.com/rbenv/ruby-build/wiki#suggested-build-environment
```
apt-get install autoconf patch build-essential rustc libssl-dev libyaml-dev libreadline6-dev zlib1g-dev libgmp-dev libncurses5-dev libffi-dev libgdbm6 libgdbm-dev libdb-dev uuid-dev
```

#### Ruby
```
git clone https://github.com/rbenv/rbenv.git ~/.rbenv
git clone https://github.com/rbenv/ruby-build.git "$(rbenv root)"/plugins/ruby-build
echo 'export PATH="$HOME/.rbenv/bin:$HOME/.rbenv/plugins/ruby-build/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(rbenv init -)"' >> ~/.bashrc
~/.rbenv/bin/rbenv init
rbenv install 3.0.2
rbenv global 3.0.2
gem install bundler —-no-document
gem install nokogiri —-no-document
```

#### Node
https://deb.nodesource.com/
```

```

## DB
_adapt with latest postgresql version_
https://www.postgresql.org/download/linux/ubuntu/
```
sudo apt-get -y install postgresql
```
_might also need libpq-dev for dev env_

adapt with latest PG
```
pg_ctlcluster 11 main start
```

```
gem install pg --no-document
```

```
gem install bcrypt --no-document
```

### on your machine

```cap staging puma:config```

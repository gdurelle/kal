require "test_helper"

class RegistrationMailerTest < ActionMailer::TestCase
  test "welcome" do
    user = users(:one)
    mail = RegistrationMailer.welcome(user, Devise.friendly_token.first(8))
    assert_equal "Welcome", mail.subject
    assert_equal [user.email], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end

# Preview all emails at http://localhost:3000/rails/mailers/registration_mailer
class RegistrationMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/registration_mailer/welcome
  def welcome
    user = User.first_or_create(email: 'test@test.com')
    RegistrationMailer.welcome(user, Devise.friendly_token.first(8))
  end

end

require 'test_helper'

class HomeControllerTest < ActionDispatch::IntegrationTest
  %w[home privacy legal tos agenda credits decentralize security].each do |page_name|
    test "should get #{page_name}" do
      get "/#{page_name}"
      assert_response :success
    end
  end
end

require 'test_helper'

class AttendeesControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @attendee = attendees(:one)
    @user = users(:one)
  end

  test 'should show agenda' do
    sign_in @attendee
    get attendee_agenda_url(slug: 'one')
    assert_response :success
  end

  test 'should NOT show agenda' do
    sign_in @user
    get attendee_agenda_url(slug: 'one')
    assert_response :redirect
  end
end

require 'test_helper'

class SlotsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @event_type = event_types(:one)
    @user_attendee = user_attendees(:one)
    @user = users(:one)
    @slot = slots(:one)
    sign_in @user
  end

  test 'should get new' do
    get new_slot_url(slug: 'one')
    assert_response :success
  end

  test 'should create slot' do
    assert_difference('Slot.count') do
      post slots_url(slug: 'one'), params: {
        slot: {
          date_start: '2019-10-28 08:04:06',
          date_end: '2019-10-28 08:04:06',
          user_id: @user.id,
          attendee: 'two',
          event_type_id: @event_type.id
        }
      }
    end

    assert_redirected_to slot_url(slug: 'one', id: Slot.last.id)
  end

  test 'should show slot' do
    get slot_url(slug: @user.slug, id: @slot.id)
    assert_response :success
  end

  test 'should get edit' do
    get edit_slot_url(slug: 'one', id: @slot.id)
    assert_response :success
  end

  test 'should update slot' do
    patch slot_url(slug: 'one', id: @slot.id), params: { slot: { id: @slot.id, date_end: '2019-10-28 09:05:00' }, slug: 'one' }
    assert_redirected_to slot_url(slug: 'one', id: @slot.id)
  end

  test 'should destroy slot' do
    assert_difference('Slot.count', -1) do
      delete slot_url(slug: 'one', id: @slot.id)
    end

    assert_redirected_to root_url
  end

  test 'should cancel slot as attendee' do
    skip 'todo'
  end

  test 'should cancel slot as user' do
    put cancel_slot_url(slug: @user.slug, id: @slot.id)
    assert @slot.attendee, nil
    assert_redirected_to user_agenda_path(@user)
  end
end

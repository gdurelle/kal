require 'test_helper'

class ServersControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  test 'should get index' do
    get servers_url, as: :json
    assert_response :success
  end
end

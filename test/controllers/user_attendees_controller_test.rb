require 'test_helper'

class UserAttendeesControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @user = users(:one)
    @user_attendee = user_attendees(:one)
    @attendee = @user_attendee.attendee
    sign_in @user
  end

  test 'should get index' do
    get user_attendees_url(user_slug: @user.slug)
    assert_response :success
  end

  test 'should get new' do
    get new_user_attendee_url(user_slug: 'one', user_attendee: @user_attendee)
    assert_response :success
  end

  test 'should post create attendeee' do
    post user_attendees_url user_slug: 'one',
                            attendee: {
                              firstname: 'newAttendeeFN',
                              lastname: 'newAttendeeLN',
                              email: 'one-attendee@test.com'
                            }
    assert_response :success
  end

  test 'should show user_attendee' do
    get user_attendee_url(user_slug: @user.slug, id: @user_attendee.id)
    assert_response :success
  end

  test 'should get edit' do
    get edit_user_attendee_url(user_slug: @user.slug, id: @user_attendee.id)
    assert_response :success
  end

  # test 'should update user_attendee' do
  #   patch user_attendee_url(@user_attendee), params: { user_attendee: { attendee_id: @user_attendee.attendee_id, user_id: @user_attendee.user_id } }
  #   assert_redirected_to user_attendee_url(@user_attendee)
  # end

  test 'should destroy user_attendee' do
    @user_attendee_to_delete = user_attendees(:two)
    assert_difference('UserAttendee.count', -1) do
      delete user_attendee_url(user_slug: @user.slug, id: @user_attendee_to_delete.id)
    end

    assert_redirected_to user_attendees_url(user_slug: @user.slug)
  end
end

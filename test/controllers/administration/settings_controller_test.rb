require 'test_helper'

module Administration
  class SettingsControllerTest < ActionDispatch::IntegrationTest
    include Devise::Test::IntegrationHelpers

    setup do
      @admin = admins(:one)
      sign_in @admin
    end

    test 'update settings' do
      patch admin_update_settings_path, params: { admin_setting: { register_pro: false, max_users: 0, register_attendee: true } }
      assert_redirected_to admin_settings_url
    end
  end
end

require 'test_helper'

module Administration
  class ServersControllerTest < ActionDispatch::IntegrationTest
    include Devise::Test::IntegrationHelpers

    setup do
      @server = servers(:one)
      sign_in admins(:one)
    end

    test 'should get index' do
      get admin_servers_url
      assert_response :success
    end

    test 'should get new' do
      get new_admin_server_url
      assert_response :success
    end

    test 'should create server' do
      assert_difference('Server.count') do
        post admin_servers_url, params: { server: { url: 'newserver', port: 3 } }
      end

      assert_redirected_to admin_servers_url
    end

    test 'should get edit' do
      get edit_admin_server_url(@server)
      assert_response :success
    end

    test 'should update server' do
      patch admin_server_url(@server), params: { server: { sync_to: true, id: 1 } }
      assert_redirected_to admin_servers_url
    end

    test 'should destroy server' do
      assert_difference('Server.count', -1) do
        delete admin_server_url(@server)
      end

      assert_redirected_to admin_servers_url
    end
  end
end

require 'test_helper'

class Administration::UsersControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @user = users(:one)
    sign_in admins(:one), scope: :admin
    @user_params = { firstname: 'John',
                     lastname: 'Doe',
                     email: 'john.doe@test.com',
                     password: 'azerty12345',
                     password_confirmation: 'azerty12345' }
  end

  test 'should get index' do
    get admin_users_url
    assert_response :success
  end

  test 'should get new' do
    get new_admin_user_url
    assert_response :success
  end

  test 'should create a user' do
    assert_difference('User.count') do
      post admin_users_url, params: { user: @user_params }
    end

    assert_redirected_to admin_users_url
  end

  test 'should get edit' do
    get edit_admin_user_url(@user)
    assert_response :success
  end

  test 'should update user' do
    patch admin_user_url(@user), params: { user: { firstname: 'Jane' } }
    assert_redirected_to admin_users_url
  end

  test 'should destroy user' do
    assert_difference('User.count', -1) do
      delete admin_user_url(@user)
    end

    assert_redirected_to admin_users_url
  end
end

require 'test_helper'

module Administration
  class DashboardControllerTest < ActionDispatch::IntegrationTest
    include Devise::Test::IntegrationHelpers

    test 'should NOT get index' do
      get admin_dashboard_url(format: :json)
      assert_response :unauthorized
    end

    test 'should NOT get settings' do
      get admin_settings_url(format: :json)
      assert_response :unauthorized
    end

    test 'should get index' do
      sign_in admins(:one)
      get admin_dashboard_url
      assert_response :success
    end

    test 'should get settings' do
      sign_in admins(:one)
      get admin_settings_url
      assert_response :success
    end
  end
end

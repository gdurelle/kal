require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @user = users(:one)
  end

  test 'should get profile' do
    get user_profile_url(user_slug: 'one')
    assert_response :success
  end

  test 'should get NOT agenda' do
    get user_agenda_url(user_slug: 'one')
    assert_response :redirect
  end
  test 'should get agenda' do
    sign_in @user
    get user_agenda_url(user_slug: 'one')
    assert_response :success
  end
end

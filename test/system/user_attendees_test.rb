require 'application_system_test_case'

class UserAttendeesTest < ApplicationSystemTestCase
  setup do
    @user_attendee = user_attendees(:one)
  end

  test 'visiting the index' do
    visit user_attendees_url
    assert_selector 'h1', text: 'User Attendees'
  end

  test 'creating a User attendee' do
    visit user_attendees_url
    click_on 'New User Attendee'

    fill_in 'Attendee', with: @user_attendee.attendee_id
    fill_in 'User', with: @user_attendee.user_id
    click_on 'Create User attendee'

    assert_text 'User attendee was successfully created'
    click_on 'Back'
  end

  test 'updating a User attendee' do
    visit user_attendees_url
    click_on 'Edit', match: :first

    fill_in 'Attendee', with: @user_attendee.attendee_id
    fill_in 'User', with: @user_attendee.user_id
    click_on 'Update User attendee'

    assert_text 'User attendee was successfully updated'
    click_on 'Back'
  end

  test 'destroying a User attendee' do
    visit user_attendees_url
    page.accept_confirm do
      click_on 'Destroy', match: :first
    end

    assert_text 'User attendee was successfully destroyed'
  end
end

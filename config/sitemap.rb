require 'rubygems'
require 'sitemap_generator'

# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = "https://kal.federa.tech"

SitemapGenerator::Sitemap.create do
  # Put links creation logic here.
  #
  add '/', :changefreq => 'weekly', :priority => 0.8
  add '/agenda', :changefreq => 'monthly', :priority => 0.5
  add '/decentralize', :changefreq => 'monthly', :priority => 0.5
  add '/security', :changefreq => 'monthly', :priority => 0.4
  add '/privacy', :changefreq => 'monthly', :priority => 0.4
  add '/credits', :changefreq => 'monthly', :priority => 0.3
  add '/tos', :changefreq => 'monthly', :priority => 0.2
  add '/legal', :changefreq => 'monthly', :priority => 0.2

  User.find_each do |user|
    add user_profile_path(user.slug), lastmod: user.updated_at, priority: 1
  end
  # The root path '/' and sitemap index file are added automatically for you.
  # Links are added to the Sitemap in the order they are specified.
  #
  # Usage: add(path, options={})
  #        (default options are used if you don't specify)
  #
  # Defaults: :priority => 0.5, :changefreq => 'weekly',
  #           :lastmod => Time.now, :host => default_host
  #
  # Examples:
  #
  # Add '/articles'
  #
  #   add articles_path, :priority => 0.7, :changefreq => 'daily'
  #
  # Add all articles:
  #
  #   Article.find_each do |article|
  #     add article_path(article), :lastmod => article.updated_at
  #   end
end

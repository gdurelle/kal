Rails.application.routes.draw do

  resources :event_types, except: %i(show)

  devise_for :attendees, controllers: { registrations: 'registrations' }
  scope "attendees" do
    get :agenda, to: 'attendees#agenda', as: :attendee_agenda
  end
  resources :attendees, only: %i() do
    resources :slots, only: %i() do
      member do
        put :cancel
      end
    end
  end

  scope module: 'administration', as: 'admin', path: 'admin' do
    get 'dashboard/index', as: :dashboard
    get 'dashboard/settings', as: :settings
    patch 'settings', to: 'settings#update', as: :update_settings
    delete 'delete_image', to: 'settings#delete_image'
    resources :servers, except: :show
    resources :users, except: :show
    authenticate :admin do
      require 'sidekiq/web'
      mount Sidekiq::Web => '/sidekiq'
    end
  end
  devise_for :admins

  resources :user, param: :slug do
    resources :user_attendees, as: :attendees
    get '/', to: 'users#profile', as: :profile
    get '/agenda', to: 'users#agenda', as: :agenda
    get '/events', to: 'users#events', as: :user_events
  end
  scope "user/:slug" do
    resources :slots, except: %i(index) do
      member do
        put :cancel
        get :book
        patch :booking
      end
    end
  end

  resources :servers, only: %i(index show), constraints: { format: :json }

  # Used by server connection of other servers
  get 'users', to: 'users#index', constraints: { format: :json }

  devise_for :users, controllers: { registrations: 'registrations' }
  devise_scope :user do
    delete 'delete_image', to: 'registrations#delete_image'
  end
  get 'home', to: 'home#index'
  get 'privacy', to: 'home#privacy', as: :privacy
  get 'legal', to: 'home#legal', as: :legal
  get 'tos', to: 'home#tos', as: :tos
  get 'credits', to: 'home#credits', as: :credits
  get 'security', to: 'home#security', as: :security
  get 'agenda', to: 'home#agenda', as: :agenda
  get 'decentralize', to: 'home#decentralize', as: :decentralize
  root to: "home#index"
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end

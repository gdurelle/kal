class MigrateDefaultValues < ActiveRecord::Migration[6.0]
  def change
    change_column_default :users, :working_days, from: nil, to: []
    change_column_default :users, :working_hours, from: nil, to: [9,10,11,14,15,16,17]
    change_column_default :users, :default_start_hour, from: nil, to: 9
    change_column_default :users, :default_end_hour, from: nil, to: 17
    change_column_default :users, :generate_slots, from: nil, to: false
    change_column_default :users, :public_slots, from: nil, to: true
    change_column_default :users, :default_slot_duration, from: 42, to: 30
  end
end

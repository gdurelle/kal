class CreateAttendees < ActiveRecord::Migration[6.0]
  def change
    create_table :attendees do |t|
      t.string :firstname, null: false
      t.string :lastname, null: false
      t.string :email
      t.string :phone

      t.timestamps
    end
  end
end

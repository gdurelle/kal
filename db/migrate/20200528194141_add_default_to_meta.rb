class AddDefaultToMeta < ActiveRecord::Migration[6.0]
  def change
    change_column :admin_settings, :twitter_site, :string, default: '@kaldotech'
    change_column :admin_settings, :twitter_creator, :string, default: '@gdurelle'
  end
end

class AddBoolsToEventType < ActiveRecord::Migration[6.0]
  def change
    add_column :event_types, :is_available, :boolean, default: true, null: false
    add_column :event_types, :is_meeting, :boolean, default: false, null: false
    add_column :event_types, :is_offtime, :boolean, default: false, null: false
  end
end

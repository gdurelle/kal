class RemoveUserRefFromAttendee < ActiveRecord::Migration[6.0]
  def change
    remove_reference :attendees, :user
  end
end

class AddMetasToSetting < ActiveRecord::Migration[6.0]
  def change
    add_column :admin_settings, :title, :string, default: 'Kal'
    add_column :admin_settings, :description, :string, default: 'Online agenda, opensource & distributed'
    add_column :admin_settings, :twitter_site, :string
    add_column :admin_settings, :twitter_creator, :string
  end
end

class AddDefaultSlotDurationToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :default_slot_duration, :integer, default: 30
  end
end

class AddDefaultHoursToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :default_start_hour, :integer
    add_column :users, :default_end_hour, :integer
  end
end

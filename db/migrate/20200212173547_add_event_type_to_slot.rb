class AddEventTypeToSlot < ActiveRecord::Migration[6.0]
  def change
    add_reference :slots, :event_type, foreign_key: true
  end
end

class ChangeUserIdOnAttendee < ActiveRecord::Migration[6.0]
  def change
    change_column :attendees, :user_id, :bigint, null: true
  end
end

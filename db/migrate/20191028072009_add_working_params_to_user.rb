class AddWorkingParamsToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :working_days, :json
    add_column :users, :working_hours, :json
  end
end

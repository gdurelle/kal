class AddBehaviourToSlot < ActiveRecord::Migration[6.0]
  def change
    add_column :slots, :on_time, :string, default: 'neutral'
    add_column :slots, :behaviour, :string, default: 'neutral'
  end
end

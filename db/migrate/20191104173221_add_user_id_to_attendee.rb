class AddUserIdToAttendee < ActiveRecord::Migration[6.0]
  def change
    add_reference :attendees, :user, null: false, foreign_key: true
  end
end

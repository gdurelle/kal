class CleanupUserHours < ActiveRecord::Migration[6.0]
  def change
    remove_column :users, :default_start_hour
    remove_column :users, :default_end_hour
  end
end

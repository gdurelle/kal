class CreateAdminSettings < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_settings do |t|
      t.boolean :register_pro, default: true
      t.boolean :register_attendee, default: true
      t.integer :max_users, default: 0

      t.timestamps
    end
  end
end

class CreateServers < ActiveRecord::Migration[6.0]
  def change
    create_table :servers do |t|
      t.string :url
      t.integer :port, default: 80
      t.boolean :sync_to
      t.boolean :sync_from

      t.timestamps
    end
  end
end

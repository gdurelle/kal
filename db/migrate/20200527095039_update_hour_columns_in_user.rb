class UpdateHourColumnsInUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :default_start_time, :time, default: Time.zone.parse('09:00')
    add_column :users, :default_end_time, :time, default: Time.zone.parse('18:00')
  end
end

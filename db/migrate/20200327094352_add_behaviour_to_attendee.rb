class AddBehaviourToAttendee < ActiveRecord::Migration[6.0]
  def change
    add_column :attendees, :on_time, :string, default: 'neutral'
    add_column :attendees, :behaviour, :string, default: 'neutral'
    add_column :attendees, :behaviour_updated_at, :datetime
  end
end

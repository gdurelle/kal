class AddCancelationStrategyToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :cancel_delay, :integer, default: 0
    add_column :users, :cancel_reopen, :boolean, default: false
  end
end

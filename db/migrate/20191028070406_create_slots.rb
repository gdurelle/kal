class CreateSlots < ActiveRecord::Migration[6.0]
  def change
    create_table :slots do |t|
      t.datetime :date_start
      t.datetime :date_end
      t.references :user, null: false, foreign_key: true
      t.references :attendee, null: true, foreign_key: true

      t.timestamps
    end
  end
end

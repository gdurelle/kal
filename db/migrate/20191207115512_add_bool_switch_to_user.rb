class AddBoolSwitchToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :generate_slots, :boolean
    add_column :users, :public_slots, :boolean
  end
end

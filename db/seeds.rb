# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# EventType
EventType::ALL.each do |basic_event_label|
  EventType.find_or_create_by(label: basic_event_label) do |event_type|
    if basic_event_label==EventType::MEETING
      event_type.color = '#42BB42'
      event_type.is_available = false
      event_type.is_meeting = true
      event_type.is_offtime = false
    elsif basic_event_label==EventType::OFFTIME
      event_type.color = '#ee4222';
      event_type.is_available = false
      event_type.is_meeting = false
      event_type.is_offtime = true
    else
      event_type.color = '#424242'
      event_type.is_available = true
      event_type.is_meeting = false
      event_type.is_offtime = false
    end
  end
end

if Rails.env != 'production'
  Admin.find_or_create_by(email: 'admin@admin.com') do |admin|
    admin.password = 'admin12345'
    admin.password_confirmation = 'admin12345'
  end
  Admin::Setting.first_or_create
else
  puts 'For your own safety, admin was not created automatically.'
  puts 'Please do it manually with a strong password'
end
